#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to be able to automate the incrementation of a prerelease version and
    and used from the ci system to publish.

    Arguments
        PRERELEASE_VERSION_ID (Required) = An identifier that should be incrementing to be used in creating a
                                           prerelease version number.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

options=$(getopt -o h --long help -- "$@")

while true; do
    case "$1" in
        -h|--help)
            echo "$(helptext)"
            exit 1
            ;;
        --)
            shift
            break
            ;;
        *)
            break
            ;;
    esac
    shift
done

PRERELEASE_VERSION_ID=${1:?"$(error 'PRERELEASE_VERSION_ID must be set')"}

INCREMENT_FLAGS="-P alpha.$PRERELEASE_VERSION_ID"

source ${BASH_SOURCE%/*}/util.sh

CURRENT_VERSION=$(get_version Cargo.toml)
echo "Retrieved existing version: $CURRENT_VERSION"
NEXT_VERSION=$(increment_version $INCREMENT_FLAGS $CURRENT_VERSION)
echo "Determined next prerelease version: $NEXT_VERSION."
set_version Cargo.toml $NEXT_VERSION
echo "Set version in Cargo.toml"
